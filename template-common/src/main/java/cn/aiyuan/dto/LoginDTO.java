package cn.aiyuan.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * @author wy
 * @date 2020/11/16
 */
@ApiModel
@Data
public class LoginDTO {

    @NotEmpty(message = "username不能为空")
    private String username;

    @NotEmpty(message = "password不能为空")
    private String password;

}
