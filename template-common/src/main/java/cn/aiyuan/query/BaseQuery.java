package cn.aiyuan.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author wy
 * @date 2020/9/9
 */
@Data
@ApiModel
public class BaseQuery {


    @ApiModelProperty("当前页")
    private long current = 1;

    @ApiModelProperty("每页显示条数")
    private long size = 10;

    @ApiModelProperty("搜索关键字")
    private String keyWord;


}
