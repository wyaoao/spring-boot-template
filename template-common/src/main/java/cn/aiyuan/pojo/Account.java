package cn.aiyuan.pojo;

import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 账户表(Account)表实体类
 *
 * @author aiyuan
 * @since 2020-11-13 17:16:11
 */
@ApiModel
@Data
@TableName(value = "account")
public class Account {


    @ApiModelProperty(value = "")
    private Long id;
    /**
     * 账号
     */
    @ApiModelProperty(value = "账号")
    private String account;
    /**
     * 用户昵称
     */
    @ApiModelProperty(value = "用户昵称")
    private String nickName;
    /**
     * 邮箱
     */
    @ApiModelProperty(value = "邮箱")
    private String email;
    /**
     * 手机号码
     */
    @ApiModelProperty(value = "手机号码")
    private String phone;
    /**
     * 加密密码
     */
    @ApiModelProperty(value = "加密密码")
    private String encryptedPassword;
    /**
     * 密码盐
     */
    @ApiModelProperty(value = "密码盐")
    private String salt;
    /**
     * 是否删除  0:未删除 1:已删除
     */
    @TableLogic
    @ApiModelProperty(value = "是否删除  0:未删除 1:已删除")
    private Boolean deleted;
    /**
     * 是否为内置
     */
    @ApiModelProperty(value = "是否为内置")
    private Boolean initial;
    /**
     * 是否启用 1 启用 0 禁用
     */
    @ApiModelProperty(value = "是否启用 1 启用 0 禁用")
    private Boolean enable;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private Date createTime;
    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人")
    private Long createId;
    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    private Date updateTime;
    /**
     * 修改人
     */
    @ApiModelProperty(value = "修改人")
    private Long updateId;

}
