package cn.aiyuan.pojo;

import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 权限表(Permission)表实体类
 * @author aiyuan
 * @since 2020-11-13 17:16:21
 */
@ApiModel
@Data
@TableName(value = "permission")
public class Permission {


    @ApiModelProperty(value = "")
    private Long id;
    /**
     * 父级id 用于展示作用 最顶级默认为0
     */
    @ApiModelProperty(value = "父级id 用于展示作用 最顶级默认为0")
    private Long parentId;
    /**
     * 权限展示名称
     */
    @ApiModelProperty(value = "权限展示名称")
    private String label;
    /**
     * 后台权限控制使用的名称
     */
    @ApiModelProperty(value = "后台权限控制使用的名称")
    private String code;
    /**
     * 访问时的url
     */
    @ApiModelProperty(value = "访问时的url")
    private String url;
    /**
     * 是否显示 用于页面是否在菜单中显示
     */
    @ApiModelProperty(value = "是否显示 用于页面是否在菜单中显示")
    private Boolean display;
    /**
     * 排序
     */
    @ApiModelProperty(value = "排序")
    private Integer sort;
    /**
     * 是否为内置权限
     */
    @ApiModelProperty(value = "是否为内置权限")
    private Boolean initial;
    /**
     * 是否启用 1 启用 0 禁用
     */
    @ApiModelProperty(value = "是否启用 1 启用 0 禁用")
    private Boolean enable;
    /**页面类型 ,菜单类型 还是接口类型 还是按钮类型 控制显隐和访问
     page
     menu
     button
     api*/
    @ApiModelProperty(value = "页面类型 ,菜单类型 还是接口类型 还是按钮类型 控制显隐和访问page menu button api")
    private String type;

    /**是否删除 0未删除 1 已删除*/
    @TableLogic
    @ApiModelProperty(value = "是否删除 0未删除 1 已删除")
    private Boolean deleted;

    /**创建时间*/
    @ApiModelProperty(value = "创建时间")
    private Date createTime;
    /**创建人*/
    @ApiModelProperty(value = "创建人")
    private Long createId;
    /**修改时间*/
    @ApiModelProperty(value = "修改时间")
    private Date updateTime;
    /**修改人*/
    @ApiModelProperty(value = "修改人")
    private Long updateId;
}
