package cn.aiyuan.pojo;

import cn.aiyuan.pojo.base.LogEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 异常日志表(ExceptionLog)表实体类
 *
 * @author aiyuan
 * @since 2020-11-13 17:16:20
 */
@ApiModel
@Data
@TableName(value = "exception_log")
public class ExceptionLog extends LogEntity {


    /**
     * 请求方法
     */
    @ApiModelProperty(value = "请求方法")
    private String method;
    /**
     * 异常名称
     */
    @ApiModelProperty(value = "异常名称")
    private String name;
    /**
     * 异常信息
     */
    @ApiModelProperty(value = "异常信息")
    private String message;

}
