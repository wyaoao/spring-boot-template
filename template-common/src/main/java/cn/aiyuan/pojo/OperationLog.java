package cn.aiyuan.pojo;

import cn.aiyuan.pojo.base.LogEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 操作日志表(OperationLog)表实体类
 *
 * @author aiyuan
 * @since 2020-11-13 17:16:20
 */
@ApiModel
@Data
@TableName(value = "operation_log")
public class OperationLog extends LogEntity {

    /**
     * 操作模块
     */
    @ApiModelProperty(value = "操作模块")
    private String module;
    /**
     * 操作类型
     */
    @ApiModelProperty(value = "操作类型")
    private String type;
    /**
     * 操作描述
     */
    @ApiModelProperty(value = "操作描述")
    private String description;


    /**
     * 请求方法
     */
    @ApiModelProperty(value = "请求方法")
    private String method;
    /**
     * 返回结果
     */
    @ApiModelProperty(value = "返回结果")
    private String respContent;

}
