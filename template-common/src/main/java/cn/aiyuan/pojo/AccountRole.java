package cn.aiyuan.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 账户和角色关联表(AccountRole)表实体类
 *
 * @author aiyuan
 * @since 2020-11-13 17:16:18
 */
@ApiModel
@Data
@TableName(value = "account_role")
public class AccountRole {


    @ApiModelProperty(value = "")
    private Long id;
    /**
     * 角色id
     */
    @ApiModelProperty(value = "角色id")
    private Long roleId;
    /**
     * 账号id
     */
    @ApiModelProperty(value = "账号id")
    private Long accountId;

}
