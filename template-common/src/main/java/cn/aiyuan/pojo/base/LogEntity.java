package cn.aiyuan.pojo.base;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Administrator
 * @className LogEntity
 * @since 2020-09-29 13:45
 */
@Data
public class LogEntity implements Serializable {

	private static final long serialVersionUID = -4491301251890707071L;

	@ApiModelProperty(name = "id", value = "id")
	@JsonProperty("id")
	private Long id;

	@ApiModelProperty(name = "appellation", value = "请求参数")
	private String requestParam;

	@ApiModelProperty(name = "func", value = "请求方法")
	private String method;

	@ApiModelProperty(name = "user", value = "请求用户ID")
	private Long userId;

	@ApiModelProperty(name = "uName", value = "请求用户名称")
	private String userName;

	@ApiModelProperty(name = "addr", value = "请求IP")
	private String ip;

	@ApiModelProperty(name = "link", value = "请求URI")
	private String uri;

	@ApiModelProperty(name = "time", value = "异常名称")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTime;

}
