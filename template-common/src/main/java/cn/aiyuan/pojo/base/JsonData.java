package cn.aiyuan.pojo.base;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 服务器响应内容类
 * @author Administrator
 * @date 2019/7/16 12:02
 */
public class JsonData implements Serializable {

	@ApiModelProperty(value = "状态码")
	private Integer code;

	@ApiModelProperty(value = "数据")
	private Object data;

	@ApiModelProperty(value = "描述")
	private String msg;

	public JsonData() {
	}

	public JsonData(Integer code, Object data, String msg) {
		this.code = code;
		this.data = data;
		this.msg = msg;
	}

	public static JsonData ok() {
		return new JsonData(ResultCode.SUCCESS.getCode(), null, "操作成功");
	}

	public static JsonData ok(Object object) {
		return new JsonData(ResultCode.SUCCESS.getCode(), object, "操作成功");
	}

	public static JsonData ok(Object object, String msg) {
		return new JsonData(ResultCode.SUCCESS.getCode(), object, msg);
	}

	public static JsonData okMsg(String msg) {
		return new JsonData(ResultCode.SUCCESS.getCode(), null, msg);
	}

	/**
	 * 操作失败
	 *
	 * @param msg 错误信息
	 * @return
	 */
	public static JsonData fail(String msg) {
		return new JsonData(ResultCode.FAILURE.getCode(), null, msg);
	}

	public static JsonData fail(Integer code,String msg) {
		return new JsonData(code, null, msg);
	}

	/**
	 * 操作失败 默认返回 '操作失败'
	 *
	 * @return
	 */
	public static JsonData fail() {
		return new JsonData(ResultCode.FAILURE.getCode(), null, "操作失败");
	}

	/**
	 * 操作失败 默认返回 '操作失败'
	 * @param object 错误数据
	 */
	public static JsonData fail(Object object) {
		return new JsonData(ResultCode.FAILURE.getCode(), object, "操作失败");
	}

	/**
	 * 未知错误
	 */
	public static JsonData unknownError() {
		return new JsonData(ResultCode.FAILURE.getCode(), null, "服务器错误,请稍后再试!");
	}

	/**
	 * 参数错误
	 *
	 * @param msg 错误信息
	 */
	public static JsonData parameterError(String msg) {
		return new JsonData(ResultCode.FAILURE.getCode(), null, msg);
	}

	/**
	 * 未登录
	 */
	public static JsonData noLogin() {
		return new JsonData(ResultCode.UN_AUTHORIZED.getCode(), null, "暂未登录或token已经过期");
	}

	/**
	 * 资源未找到
	 *
	 */
	public static JsonData notFind() {
		return new JsonData(ResultCode.NOT_FOUND.getCode(), null, "资源未找到");
	}

	/**
	 * 没有访问权限
	 *
	 */
	public static JsonData unauthorized() {
		return new JsonData(ResultCode.UN_AUTHORIZED.getCode(), null, "没有访问权限");
	}

	/**
	 * 没有访问权限
	 *
	 */
	public static JsonData unauthorized(String msg) {
		return new JsonData(ResultCode.UN_AUTHORIZED.getCode(), null, msg);
	}

	/**
	 * 服务错误
	 *
	 */
	public static JsonData serviceError(String msg) {
		return new JsonData(ResultCode.INTERNAL_SERVER_ERROR.getCode(), null, msg);
	}

	/**
	 * 重复提交
	 */
	public static JsonData repeatSubmit() {
		return new JsonData(ResultCode.REPEATED_SUBMIT.getCode(), null, "请勿重复提交");
	}

	/**
	 * 自定义返回数据
	 *
	 * @param obj
	 */
	public JsonData data(Object obj) {
		setData(obj);
		return this;
	}

	/**
	 * 自定义状态信息
	 */
	public JsonData message(String message) {
		setMsg(message);
		return this;
	}

	/**
	 * 自定义状态码
	 */
	public JsonData code(Integer code) {
		setCode(code);
		return this;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	@Override
	public String toString() {
		return "JsonData [code=" + code + ", data=" + data + ", msg=" + msg + "]";
	}

}
