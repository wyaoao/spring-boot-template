package cn.aiyuan.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 账户和权限中间表(AccountPermission)表实体类
 *
 * @author aiyuan
 * @since 2020-11-13 17:16:17
 */
@ApiModel
@Data
@TableName(value = "account_permission")
public class AccountPermission {


    @ApiModelProperty(value = "")
    private Long id;
    /**
     * 角色id
     */
    @ApiModelProperty(value = "角色id")
    private Long accountId;
    /**
     * 权限id
     */
    @ApiModelProperty(value = "权限id")
    private Long permissionId;
    /**
     * 是否被选中 true为全选 false为半选
     */
    @ApiModelProperty(value = "是否被选中 true为全选 false为半选")
    private Boolean checked;

}
