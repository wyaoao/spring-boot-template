package cn.aiyuan.pojo;

import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 字典表
 * (Dictionary)表实体类
 *
 * @author aiyuan
 * @since 2020-11-13 17:16:19
 */
@ApiModel
@Data
@TableName(value = "dictionary")
public class Dictionary {

    /**
     * rid
     */
    @ApiModelProperty(value = "rid")
    private Long id;
    /**
     * 父id
     */
    @ApiModelProperty(value = "父id")
    private Long parentId;
    /**
     * 字典码
     */
    @ApiModelProperty(value = "字典码")
    private String code;
    /**
     * 字典名称
     */
    @ApiModelProperty(value = "字典名称")
    private String name;
    /**
     * 字典项编码
     */
    @ApiModelProperty(value = "字典项编码")
    private String subCode;
    /**
     * 字典项名称
     */
    @ApiModelProperty(value = "字典项名称")
    private String subName;
    /**
     * 附加字段1
     */
    @ApiModelProperty(value = "附加字段1")
    private String attach1;
    /**
     * 附加字段2
     */
    @ApiModelProperty(value = "附加字段2")
    private String attach2;
    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;
    /**
     * 是否删除  0:未删除 1:已删除
     */
    @TableLogic
    @ApiModelProperty(value = "是否删除  0:未删除 1:已删除")
    private Boolean deleted;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private Date createTime;
    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人")
    private Long createId;
    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    private Date updateTime;
    /**
     * 修改人
     */
    @ApiModelProperty(value = "修改人")
    private Long updateId;

}
