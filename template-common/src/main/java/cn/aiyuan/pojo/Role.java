package cn.aiyuan.pojo;

import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 角色表(Role)表实体类
 *
 * @author aiyuan
 * @since 2020-11-13 17:16:22
 */
@ApiModel
@Data
@TableName(value = "role")
public class Role {


    @ApiModelProperty(value = "")
    private Long id;
    /**
     * 角色简称
     */
    @ApiModelProperty(value = "角色简称")
    private String code;
    /**
     * 角色名称
     */
    @ApiModelProperty(value = "角色名称")
    private String name;
    /**
     * 角色描述
     */
    @ApiModelProperty(value = "角色描述")
    private String description;
    /**
     * 是否删除。默认0：未删除；1：已删除
     */
    @TableLogic
    @ApiModelProperty(value = "是否删除。默认0：未删除；1：已删除")
    private Boolean deleted;
    /**
     * 是否为内置角色
     */
    @ApiModelProperty(value = "是否为内置角色")
    private Boolean initial;
    /**
     * 是否为默认角色
     */
    @ApiModelProperty(value = "是否为默认角色")
    private Boolean def;
    /**
     * 是否启用 1 启用 0 禁用
     */
    @ApiModelProperty(value = "是否启用 1 启用 0 禁用")
    private Boolean enable;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private Date createTime;
    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人")
    private Long createId;
    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    private Date updateTime;
    /**
     * 修改人
     */
    @ApiModelProperty(value = "修改人")
    private Long updateId;

}
