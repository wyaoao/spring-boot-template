package cn.aiyuan.controller;

import cn.aiyuan.common.annotation.OperationLogAnno;
import cn.aiyuan.common.enumerate.OperationTypeEnum;
import cn.aiyuan.pojo.Dictionary;
import cn.aiyuan.pojo.base.JsonData;
import cn.aiyuan.service.DictionaryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 字典表 (Dictionary)表控制层
 *
 * @author aiyuan
 * @since 2020-09-18 13:50:27
 */
@Slf4j
@Validated
@Api(value = "字典表  模块接口", tags = "字典表  模块接口")
@RestController
@RequestMapping("/api/v1/dictionary")
public class DictionaryController {

	/**
	 * 服务对象
	 */
	@Resource
	private DictionaryService dictionaryService;

	@ApiOperation(value = "字典树", notes = "获取整个字典树")
	@GetMapping("dictionaryTree")
	@OperationLogAnno(module = "字典模块", type = OperationTypeEnum.RETRIEVE, desc = "获取整个字典树")
	JsonData getDictionaryTree() {
		return JsonData.ok();
	}

	/**
	 * @author 痴
	 * @since 2020-09-18 13:50:27
	 */
	@GetMapping("/selectOne")
	@ApiOperation(value = "查询单条数据", notes = "通过主键查询Dictionary表单条数据")
	@OperationLogAnno(module = "字典模块", type = OperationTypeEnum.RETRIEVE, desc = "查询单条数据")
	public JsonData selectOne(
			@ApiParam(name = "id", value = "Dictionary表id", required = true) @NotNull(message = "id不能为空") Long id) {
		return JsonData.ok(dictionaryService.getById(id));
	}

	/**
	 * @author 痴
	 * @since 2020-09-18 13:50:27
	 */
	@GetMapping("/queryAll")
	@ApiOperation(value = "查询全部数据", notes = "可通过dictionaryDO对象包含的参数进行数据筛选")
	@OperationLogAnno(module = "字典模块", type = OperationTypeEnum.RETRIEVE, desc = "查询全部数据")
	public JsonData queryAll() {
		return JsonData.ok();
	}

	/**
	 * @author 痴
	 * @since 2020-09-18 13:50:27
	 */
	@ApiOperation(value = "查询分页数据", notes = "可通过Pagination对象传递分页信息及其他参数进行数据筛选")
	@GetMapping("/queryPageList")
	@OperationLogAnno(module = "字典模块", type = OperationTypeEnum.RETRIEVE, desc = "查询分页数据")
	public JsonData queryPageList() {
		return JsonData.ok();
	}

	/**
	 * @author 痴
	 * @since 2020-09-18 13:50:27
	 */
	@ApiOperation(value = "新增数据", notes = "新增一条Dictionary表数据")
	@PostMapping("/insert")
	@OperationLogAnno(module = "字典模块", type = OperationTypeEnum.CHECK, desc = "新增数据")
	public JsonData insert(@RequestBody Dictionary dictionary) {
		dictionaryService.save(dictionary);
		return JsonData.ok();
	}

	/**
	 * @since 2020-09-18 13:50:27
	 */
	@ApiOperation(value = "修改数据", notes = "修改一条Dictionary表数据")
	@PutMapping("/update")
	@OperationLogAnno(module = "字典模块", type = OperationTypeEnum.UPDATE, desc = "修改数据")
	public JsonData update(@RequestBody Dictionary dictionary) {
		dictionaryService.updateById(dictionary);
		return JsonData.ok();
	}

	/**
	 * @since 2020-09-18 13:50:27
	 */
	@ApiOperation(value = "删除数据", notes = "通过主键假删除一条Dictionary表数据")
	@DeleteMapping("/deleteById")
	@OperationLogAnno(module = "字典模块", type = OperationTypeEnum.DELETE, desc = "删除数据")
	public JsonData deleteById(@ApiParam(name = "id", value = "Dictionary表id", required = true) Long id) {
		dictionaryService.removeById(id);
		return JsonData.ok();
	}

	/**
	 * @since 2020-09-18 13:50:27
	 */
	@ApiOperation(value = "批量删除数据", notes = "通过主键集合批量假删除Dictionary表数据")
	@DeleteMapping("/batchDeletionByIdList")
	@OperationLogAnno(module = "字典模块", type = OperationTypeEnum.DELETE, desc = "批量删除数据")
	public JsonData batchDeletionByIdList(@RequestParam List<Long> idList) {
		dictionaryService.removeByIds(idList);
		return JsonData.ok();
	}

}
