package cn.aiyuan.controller;

import cn.aiyuan.common.annotation.OperationLogAnno;
import cn.aiyuan.pojo.Permission;
import cn.aiyuan.pojo.base.JsonData;
import cn.aiyuan.query.BaseQuery;
import cn.aiyuan.service.PermissionService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 后台用户权限表
 * </p>
 *
 * @author zscat
 * @since 2019-04-19
 */
@Slf4j
@RestController
@Api(tags = "SysPermissionController", description = "后台用户权限表管理")
@RequestMapping("/sys/SysPermission")
public class PermissionController {
    @Resource
    private PermissionService permissionService;

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @OperationLogAnno(module = "sys", desc = "根据条件查询所有后台用户权限表列表")
    @ApiOperation("根据条件查询所有后台用户权限表列表")
    @GetMapping(value = "/list")
    @PreAuthorize("hasAuthority('sys:SysPermission:read')")
    public JsonData getRoleByPage(BaseQuery query) {
        Page<Permission> page = permissionService.page(new Page<>(query.getCurrent(), query.getSize()));
        return JsonData.ok(page);
    }

}
