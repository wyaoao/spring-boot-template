package cn.aiyuan.constant;

import lombok.Getter;

/**
 * @author Administrator
 */
public enum GlobalConstant {
    //登录
    LOGIN("Login"),

    //退出登录
    LOGOUT("logout"),

    //USER_TOKEN
    USER_TOKEN("user:")

    ;

    @Getter
    private final String value;

    GlobalConstant(String logout) {
        value = logout;
    }
}
