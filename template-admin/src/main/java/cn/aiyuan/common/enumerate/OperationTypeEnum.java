package cn.aiyuan.common.enumerate;

/**
 * 日志操作类型
 *
 * @author 痴
 * @className OperationTypeEnum
 * @since 2020-09-29 14:02
 */
public enum OperationTypeEnum {
	/**
	 * 增加
	 */
	CREATE,
	/**
	 * 查询
	 */
	RETRIEVE,
	/**
	 * 修改
	 */
	UPDATE,
	/**
	 * 删除
	 */
	DELETE,
	/**
	 * 上传
	 */
	UPLOAD,
	/**
	 * 下载
	 */
	DOWNLOAD,
	/**
	 * 校验
	 */
	CHECK,
	/**
	 * 其他
	 */
	OTHER;
}
