package cn.aiyuan.common.enumerate;

/**
 * 自定义限流类型
 *
 * @author 痴
 * @className LimitTypeEnum
 * @since 2020-04-16 10:19
 */
public enum LimitTypeEnum {
	/**
	 * 9     * 自定义key 10
	 */
	CUSTOMER,

	/**
	 * 请求者IP
	 */
	IP;
}
