package cn.aiyuan.common.exception.error;



import cn.aiyuan.common.exception.ServiceException;
import cn.aiyuan.pojo.base.JsonData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;


/**
 * 自定义的全局异常处理
 * @author Administrator
 */

@RestControllerAdvice
public class JcRestExceptionTranslator {
    private static final Logger log = LoggerFactory.getLogger(JcRestExceptionTranslator.class);

    public JcRestExceptionTranslator() {
    }

    @ExceptionHandler({ServiceException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public JsonData handleError(ServiceException e) {
        log.error("业务异常", e);
        return JsonData.fail(e.getResultCode().getCode(), e.getMessage());
    }
}

