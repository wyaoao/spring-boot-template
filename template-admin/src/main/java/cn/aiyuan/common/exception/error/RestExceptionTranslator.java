package cn.aiyuan.common.exception.error;



import cn.aiyuan.pojo.base.ResultCode;
import cn.aiyuan.pojo.base.JsonData;
import org.hibernate.validator.internal.engine.path.PathImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication.Type;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.Servlet;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Set;

/**
 * rest的异常
 * @author THUNOEROBOT
 */

@Order(-2147483648)
@Configuration
@ConditionalOnWebApplication(type = Type.SERVLET)
@ConditionalOnClass({Servlet.class, DispatcherServlet.class})
@RestControllerAdvice
public class RestExceptionTranslator {
    private static final Logger log = LoggerFactory.getLogger(RestExceptionTranslator.class);

    public RestExceptionTranslator() {
    }

    @ExceptionHandler({MissingServletRequestParameterException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public JsonData handleError(MissingServletRequestParameterException e) {
        log.warn("缺少请求参数{}", e.getMessage());
        String message = String.format("缺少必要的请求参数: %s", e.getParameterName());
        return JsonData.fail(ResultCode.PARAM_MISS.getCode(), message);
    }

    @ExceptionHandler({MethodArgumentTypeMismatchException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public JsonData handleError(MethodArgumentTypeMismatchException e) {
        log.warn("请求参数格式错误{}", e.getMessage());
        String message = String.format("请求参数格式错误: %s", e.getName());
        return JsonData.fail(ResultCode.PARAM_TYPE_ERROR.getCode(), message);
    }

    @ExceptionHandler({MethodArgumentNotValidException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public JsonData handleError(MethodArgumentNotValidException e) {
        log.warn("参数验证失败{}", e.getMessage());
        return this.handleError(e.getBindingResult());
    }

    @ExceptionHandler({BindException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public JsonData handleError(BindException e) {
        log.warn("参数绑定失败{}", e.getMessage());
        return this.handleError(e.getBindingResult());
    }

    private JsonData handleError(BindingResult result) {
        FieldError error = result.getFieldError();
        String message = String.format("%s:%s", error.getField(), error.getDefaultMessage());
        return JsonData.fail(ResultCode.PARAM_BIND_ERROR.getCode(), message);
    }

    @ExceptionHandler({ConstraintViolationException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public JsonData handleError(ConstraintViolationException e) {
        log.warn("参数验证失败{}", e.getMessage());
        Set<ConstraintViolation<?>> violations = e.getConstraintViolations();
        ConstraintViolation<?> violation = violations.iterator().next();
        String path = ((PathImpl)violation.getPropertyPath()).getLeafNode().getName();
        String message = String.format("%s:%s", path, violation.getMessage());
        return JsonData.fail(ResultCode.PARAM_VALID_ERROR.getCode(), message);
    }

    @ExceptionHandler({NoHandlerFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public JsonData handleError(NoHandlerFoundException e) {
        log.error("404没找到请求:{}", e.getMessage());
        return JsonData.fail(ResultCode.NOT_FOUND.getCode(), e.getMessage());
    }

    @ExceptionHandler({HttpMessageNotReadableException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public JsonData handleError(HttpMessageNotReadableException e) {
        log.error("消息不能读取:{}", e.getMessage());
        return JsonData.fail(ResultCode.MSG_NOT_READABLE.getCode(), e.getMessage());
    }

    @ExceptionHandler({HttpRequestMethodNotSupportedException.class})
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    public JsonData handleError(HttpRequestMethodNotSupportedException e) {
        log.error("不支持当前请求方法:{}", e.getMessage());
        return JsonData.fail(ResultCode.METHOD_NOT_SUPPORTED.getCode(), e.getMessage());
    }

    @ExceptionHandler({HttpMediaTypeNotSupportedException.class})
    @ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
    public JsonData handleError(HttpMediaTypeNotSupportedException e) {
        log.error("不支持当前媒体类型:{}", e.getMessage());
        return JsonData.fail(ResultCode.MEDIA_TYPE_NOT_SUPPORTED.getCode(), e.getMessage());
    }

}
