package cn.aiyuan.common.exception;


import cn.aiyuan.pojo.base.ResultCode;

/**
 * @author THUNOEROBOT
 * 定义异常
 */
public class ServiceException extends RuntimeException {

    private static final long serialVersionUID = 2359767895161832954L;

    private final ResultCode resultCode;

    public ServiceException(String message) {
        super(message);
        this.resultCode = ResultCode.FAILURE;
    }

    public ServiceException(ResultCode resultCode) {
        super(resultCode.getMessage());
        this.resultCode = resultCode;
    }

    public ServiceException(ResultCode resultCode, Throwable cause) {
        super(cause);
        this.resultCode = resultCode;
    }

    @Override
    public Throwable fillInStackTrace() {
        return this;
    }

    public Throwable doFillInStackTrace() {
        return super.fillInStackTrace();
    }

    public ResultCode getResultCode() {
        return this.resultCode;
    }
}
