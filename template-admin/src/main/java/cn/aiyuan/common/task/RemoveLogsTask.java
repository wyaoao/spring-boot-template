package cn.aiyuan.common.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author aiyuan
 * @className RemoveLogsTask
 * @since 2020-10-25 01:47
 */
@Component
@Slf4j
public class RemoveLogsTask {


	/**
	 * 每周日 凌晨删除 一周前的日志信息
	 *
	 * @since 2020/10/25 2:03
	 */
	@Scheduled(cron = "0 0 0 ? * 1")
	public void deleteLogsFrom1WeekAgo() {

		log.info("执行定时任务:deleteLogsFrom1WeekAgo");
	}

}
