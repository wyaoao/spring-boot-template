package cn.aiyuan.common.annotation;


import cn.aiyuan.common.enumerate.OperationTypeEnum;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 自定义操作日志注解
 * @author aiyuan
 * @className OperLog
 * @since 2020-09-29 10:47
 */
@Target(ElementType.METHOD) //注解放置的目标位置,METHOD是可注解在方法级别上
@Retention(RetentionPolicy.RUNTIME) //注解在哪个阶段执行
@Documented
public @interface OperationLogAnno {

	/**
	 * 操作模块
	 */
	String module() default "";

	/**
	 * 操作类型
	 */
	OperationTypeEnum type() default OperationTypeEnum.RETRIEVE;

	/**
	 * 操作说明
	 */
	String desc() default "";

}
