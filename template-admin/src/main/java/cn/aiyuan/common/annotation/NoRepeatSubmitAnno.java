package cn.aiyuan.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 禁止重复提交
 *
 * @author Administrator
 * @className NoRepeatSubmit
 * @since 2020-04-13 14:37
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface NoRepeatSubmitAnno {

	/**
	 * 设置请求锁定时间
	 */
	long lockTime() default 5L;

}
