package cn.aiyuan.common.annotation;


import cn.aiyuan.common.enumerate.LimitTypeEnum;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 自定义限流注解
 * @author Administrator
 * @className LimitAnno
 * @since 2020-04-16 10:40
 */
@Inherited
@Target(ElementType.METHOD)
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface LimitAnno {

	/**
	 * 名字
	 */
	String name() default "";

	/**
	 * key
	 */
	String key() default "";

	/**
	 * Key的前缀
	 */
	String prefix() default "limit_";

	/**
	 * 给定的时间范围 单位(秒) 多少秒内限流
	 */
	int period();

	/**
	 * 一定时间内最多访问次数 指定秒内限流多少次
	 */
	int count();

	/**
	 * 限流的类型(用户自定义key 或者 请求ip)
	 */
	LimitTypeEnum limitType();

}
