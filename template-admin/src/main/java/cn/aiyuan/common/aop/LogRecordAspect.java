package cn.aiyuan.common.aop;


import cn.aiyuan.common.annotation.OperationLogAnno;
import cn.aiyuan.common.enumerate.OperationTypeEnum;
import cn.aiyuan.common.util.CommonUtils;
import cn.aiyuan.common.util.JsonUtils;
import cn.aiyuan.pojo.ExceptionLog;
import cn.aiyuan.pojo.OperationLog;
import cn.aiyuan.pojo.base.JsonData;
import cn.aiyuan.pojo.base.LogEntity;
import cn.aiyuan.service.ExceptionLogService;
import cn.aiyuan.service.OperationLogService;
import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;

/**
 * 全局日志
 *
 * @author 痴
 * @className LogRecordAspect
 * @since 2020-07-15 09:19
 */
@Aspect
@Component
@Slf4j
public class LogRecordAspect {

	@Resource
	private OperationLogService operationLogService;

	@Resource
	private ExceptionLogService exceptionLogService;

	/**
	 * 设置操作日志切入点 记录操作日志 在注解的位置切入代码
	 */
	@Pointcut("@annotation(operationLogAnno)")
	public void operLogPointCut(OperationLogAnno operationLogAnno) {
	}

	/**
	 * 设置操作异常切入点记录异常日志 扫描所有controller包下操作
	 */
	@Pointcut("execution(* cn.aiyuan.controller..*.*(..))")
	public void excludeService() {
	}


	@AfterReturning(value = "operLogPointCut(operationLogAnno)", returning = "keys", argNames = "joinPoint,keys,operationLogAnno")
	public void doAround(JoinPoint joinPoint, Object keys, OperationLogAnno operationLogAnno) {


		OperationLog operationLog = new OperationLog();

		setRequestParamsAndMethodName(operationLog, joinPoint);
		operationLog.setId(CommonUtils.generateId());
		if (keys instanceof JsonData) {
			// 返回结果
			operationLog.setRespContent(StrUtil.sub(JsonUtils.objectToJson(keys), 0, 1000));
		}
		else {
			// 返回结果
			operationLog.setRespContent("返回格式非JsonData");
		}
		if (operationLogAnno != null) {
			String module = operationLogAnno.module();
			OperationTypeEnum type = operationLogAnno.type();
			String desc = operationLogAnno.desc();
			// 操作模块
			operationLog.setModule(module);
			// 操作类型
			operationLog.setType(type.toString());
			// 操作描述
			operationLog.setDescription(desc);
		}
		// 请求用户ID
		operationLog.setUserId(0L);
		// 请求用户名称
		operationLog.setUserName("");
		// 请求IP
		operationLog.setIp(CommonUtils.getClientIp());

		operationLogService.save(operationLog);
	}

	/**
	 * 异常返回通知，用于拦截异常日志信息 连接点抛出异常后执行
	 *
	 * @param joinPoint 切入点
	 * @param e         异常信息
	 */
	@AfterThrowing(pointcut = "excludeService()", throwing = "e")
	public void saveExceptionLog(JoinPoint joinPoint, Throwable e) {


		ExceptionLog exceptionLog = new ExceptionLog();
		setRequestParamsAndMethodName(exceptionLog, joinPoint);
		exceptionLog.setId(CommonUtils.generateId());

		// 异常名称
		exceptionLog.setName(e.getClass().getName());
		// 异常信息
		exceptionLog.setMessage(StrUtil.sub(stackTraceToString(e.getClass().getName(), e.getMessage(), e.getStackTrace()), 0, 1000));
		// 操作员ID
		exceptionLog.setUserId(0L);
		// 操作员名称
		exceptionLog.setUserName("");
		// 操作员IP
		exceptionLog.setIp(CommonUtils.getClientIp());
		exceptionLogService.save(exceptionLog);

	}

	/**
	 * 转换异常信息为字符串
	 *
	 * @param exceptionName    异常名称
	 * @param exceptionMessage 异常信息
	 * @param elements         堆栈信息
	 */
	public String stackTraceToString(String exceptionName, String exceptionMessage, StackTraceElement[] elements) {
		StringBuilder sb = new StringBuilder();
		sb.append(exceptionName).append(":").append(exceptionMessage).append("\n\t");
		for (StackTraceElement stet : elements) {
			sb.append(stet).append("\n");
		}
		return sb.toString();
	}

	/**
	 * 封装 日志信息
	 *
	 * @param logEntity 日志对象
	 * @param joinPoint 切入点
	 */
	private void setRequestParamsAndMethodName(LogEntity logEntity, JoinPoint joinPoint) {
		RequestAttributes ra = RequestContextHolder.getRequestAttributes();
		ServletRequestAttributes sra = (ServletRequestAttributes) ra;
		assert sra != null;
		HttpServletRequest request = sra.getRequest();
		String httpMethod = request.getMethod();
		String queryString = request.getQueryString();
		Object[] args = joinPoint.getArgs();
		String params = "";
		if (args.length > 0) {
			if ("GET".equals(httpMethod) || "DELETE".equals(httpMethod)) {
				params = queryString;
			}
			else {
				for (int i = 0; i < args.length; i++) {
					if (args[i] instanceof ServletRequest) {
						args[i] = ((HttpServletRequest) args[i]).getParameterMap();
					}
					else if (args[i] instanceof ServletResponse) {
						args[i] = "response对象";
					}
					else if (args[i] instanceof MultipartFile) {
						args[i] = "MultipartFile对象";
					}
					else if (args[i] instanceof MultipartFile[]) {
						args[i] = "MultipartFile数组对象";
					}
					else if (args[i] instanceof BindingResult) {
						args[i] = "BindingResult对象";
					}
				}
				params = JsonUtils.objectToJson(args);
			}
		}
		// 从切面织入点处通过反射机制获取织入点处的方法
		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		// 获取切入点所在的方法
		Method method = signature.getMethod();
		// 获取请求的类名
		String className = joinPoint.getTarget().getClass().getName();
		// 获取请求的方法名
		String methodName = className + "." + method.getName();
		// 请求参数
		logEntity.setRequestParam(params);
		// 请求方法名
		logEntity.setMethod(methodName);
		// 操作URI
		logEntity.setUri(request.getRequestURI());

	}

}
