package cn.aiyuan.common.aop;


import cn.aiyuan.common.annotation.LimitAnno;
import cn.aiyuan.common.enumerate.LimitTypeEnum;
import cn.aiyuan.common.exception.ServiceException;
import cn.aiyuan.common.util.CommonUtils;
import com.google.common.collect.ImmutableList;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.data.redis.core.script.RedisScript;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.lang.reflect.Method;

/**
 * @author Administrator
 */
@Aspect
@Component
@Slf4j
public class LimitAspect {

    private static final String UNKNOWN = "unknown";

    @Resource
    private RedisTemplate redisTemplate;

    @Pointcut("@annotation(limitAnno)")
        public void limitPointCut(LimitAnno limitAnno) {
    }

    @Around(value = "limitPointCut(limitAnno)", argNames = "pjp,limitAnno")
    public Object interceptor(ProceedingJoinPoint pjp, LimitAnno limitAnno) {
        MethodSignature signature = (MethodSignature) pjp.getSignature();
        Method method = signature.getMethod();
        LimitTypeEnum limitType = limitAnno.limitType();
        String key;
        int limitPeriod = limitAnno.period();
        int limitCount = limitAnno.count();

        //根据限流类型获取不同的key ,如果不传我们会以方法名作为key
        switch (limitType) {
            case IP:
                key = CommonUtils.getClientIp();
                break;
            case CUSTOMER:
                key = limitAnno.key();
                break;
            default:
                key = StringUtils.upperCase(method.getName());
        }

        ImmutableList<String> keys = ImmutableList.of(StringUtils.join(limitAnno.prefix(), key));
        try {
            String luaScript = buildLuaScript();
            RedisScript<Number> redisScript = new DefaultRedisScript<>(luaScript, Number.class);
            Number count = (Number) redisTemplate.execute(redisScript, keys, limitCount, limitPeriod);
            log.info("Access try count is {}  and key = {}", count, key);
            if (count != null && count.intValue() <= limitCount) {
                return pjp.proceed();
            }
            else {
                throw new ServiceException("服务器繁忙,请稍后再试!");
            }
        } catch (Throwable e) {
            if (e instanceof RuntimeException) {
                throw new RuntimeException(e.getLocalizedMessage());
            }
            throw new RuntimeException("server exception");
        }
    }

    /**
     * @description 编写 redis Lua 限流脚本
     * @since  2020/4/8 13:24
     */
    public String buildLuaScript() {
        return "local c" +
                "\nc = redis.call('get',KEYS[1])" +
                // 调用不超过最大值，则直接返回
                "\nif c and tonumber(c) > tonumber(ARGV[1]) then" +
                "\nreturn c;" +
                "\nend" +
                // 执行计算器自加
                "\nc = redis.call('incr',KEYS[1])" +
                // 从第一次调用开始限流，设置对应键值的过期
                "\nif tonumber(c) == 1 then" +
                "\nredis.call('expire',KEYS[1],ARGV[2])" +
                "\nend" +
                "\nreturn c;";
    }


}
