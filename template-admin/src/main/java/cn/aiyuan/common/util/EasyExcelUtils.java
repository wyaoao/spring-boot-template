package cn.aiyuan.common.util;

import com.alibaba.excel.EasyExcel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

/**
 * EasyExcel 相关工具类
 *
 * @author 痴
 * @since 2020/8/18 14:19
 */
public class EasyExcelUtils {

	private static final Logger log = LoggerFactory.getLogger(EasyExcelUtils.class);

	/**
	 * 读取某个 sheet 的 Excel
	 *
	 * @param excel 文件
	 * @param clazz 实体类映射
	 * @return Excel 数据 list
	 */
	public static <S> List readExcel(MultipartFile excel, Class<S> clazz) throws IOException {
		return readExcel(excel, clazz, 0, 1);
	}

	/**
	 * 读取某个 sheet 的 Excel
	 *
	 * @param excel         文件
	 * @param clazz         实体类映射
	 * @param sheetNo       sheet 的序号 从0开始
	 * @param headRowNumber 表头行数，默认为1
	 * @return Excel 数据 list
	 */
	public static <S> List<S> readExcel(MultipartFile excel, Class<S> clazz, int sheetNo, int headRowNumber) throws IOException {
		return EasyExcel.read(excel.getInputStream()).head(clazz).sheet(sheetNo).headRowNumber(headRowNumber).doReadSync();
	}

	/**
	 * 读取某个 sheet 的 Excel
	 *
	 * @param excelInputStream 文件
	 * @param clazz            实体类映射
	 * @param sheetNo          sheet 的序号 从0开始
	 * @param headRowNumber    表头行数，默认为1
	 * @return Excel 数据 list
	 */
	public static <S> List<S> readExcel(InputStream excelInputStream, Class<S> clazz, int sheetNo, int headRowNumber) throws IOException {
		return EasyExcel.read(excelInputStream).head(clazz).sheet(sheetNo).headRowNumber(headRowNumber).doReadSync();
	}

	/**
	 * 读取多个 sheet
	 *
	 * @param excel 文件
	 * @param clazz 实体类映射
	 * @return Excel 数据 list
	 * @throws IOException
	 */
	public static <S> List<S> readAllExcel(MultipartFile excel, Class<S> clazz) throws IOException {
		return EasyExcel.read(excel.getInputStream()).head(clazz).doReadAllSync();
	}

	/**
	 * 生成excel
	 *
	 * @param filePath  绝对路径, 如：/home/chenmingjian/Downloads/aaa.xlsx
	 * @param clazz     实体类映射
	 * @param sheetName sheet名称
	 * @param data      数据源
	 */
	public static <S> void writeBySimple(String filePath, Class<S> clazz, String sheetName, List<S> data) {
		EasyExcel.write(filePath, clazz).sheet(sheetName).doWrite(data);
	}

	/**
	 * 生成excel
	 *
	 * @param filePath         绝对路径, 如：/home/chenmingjian/Downloads/aaa.xlsx
	 * @param clazz            实体类映射
	 * @param templateFileName 模板路径
	 * @param data             数据源
	 */
	public static <S> void writeWithTemplate(String filePath, String templateFileName, Class<S> clazz, List<S> data) {
		EasyExcel.write(filePath, clazz).withTemplate(templateFileName).sheet().doWrite(data);
	}


	/**
	 * 生成excel到响应流 请手动关闭输出流
	 *
	 * @param outputStream     输出流
	 * @param clazz            实体类映射
	 * @param templateFileName 模板路径
	 * @param data             数据源
	 */
	public static <S> void writeWithTemplate(OutputStream outputStream, String templateFileName, Class<S> clazz, List<S> data) {
		EasyExcel.write(outputStream, clazz).withTemplate(templateFileName).sheet().doWrite(data);
	}

}
