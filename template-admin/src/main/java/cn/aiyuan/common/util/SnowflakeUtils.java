package cn.aiyuan.common.util;

import cn.hutool.core.util.IdUtil;

/**
 * 雪花算法生成id
 *
 * @author 痴
 * @className IdGenerator
 * @since 2020-04-12 13:35
 */
public class SnowflakeUtils {

	/**
	 * 工作机器ID(0~31)
	 */
	private final static long WORKER_ID = 1L;

	/**
	 * 数据中心ID(0~31)
	 */
	private final static long DATACENTER_ID = 1L;

	/**
	 * 获取雪花算法id
	 *
	 * @return
	 */
	public static long getSnowflakeId() {
		return snowflakeId(WORKER_ID, DATACENTER_ID);
	}

	/**
	 * 获取雪花算法id
	 *
	 * @param workerId     工作机器ID
	 * @param dataCenterId 数据中心ID
	 * @return
	 */
	public static long snowflakeId(long workerId, long dataCenterId) {
		return IdUtil.getSnowflake(workerId, dataCenterId).nextId();
	}

}
