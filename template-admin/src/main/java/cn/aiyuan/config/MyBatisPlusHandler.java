package cn.aiyuan.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;


/**
 * @author wangyuan
 */
@Component
public class MyBatisPlusHandler implements MetaObjectHandler {


    @Override
    public void insertFill(MetaObject metaObject) {
        //this.setFieldValByName("createUserId", SecurityHelper.getCurrentUserId(), metaObject);
        //this.setFieldValByName("createTime",new Date(), metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        //this.setFieldValByName("updateTime", new Date(), metaObject);
    }
}
