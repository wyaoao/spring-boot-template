package cn.aiyuan.service;

import cn.aiyuan.pojo.Dictionary;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 字典表
 * (Dictionary)表服务接口
 *
 * @author aiyuan
 * @since 2020-11-13 17:16:19
 */
public interface DictionaryService extends IService<Dictionary> {

}
