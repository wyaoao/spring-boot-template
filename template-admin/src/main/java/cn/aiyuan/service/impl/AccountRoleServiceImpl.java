package cn.aiyuan.service.impl;

import cn.aiyuan.mapper.AccountRoleMapper;
import cn.aiyuan.pojo.AccountRole;
import cn.aiyuan.service.AccountRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 账户和角色关联表(AccountRole)表服务实现类
 *
 * @author aiyuan
 * @since 2020-11-13 17:16:18
 */
@Service
public class AccountRoleServiceImpl extends ServiceImpl<AccountRoleMapper, AccountRole> implements AccountRoleService {

}
