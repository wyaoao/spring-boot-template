package cn.aiyuan.service.impl;

import cn.aiyuan.mapper.DictionaryMapper;
import cn.aiyuan.pojo.Dictionary;
import cn.aiyuan.service.DictionaryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 字典表
 * (Dictionary)表服务实现类
 *
 * @author aiyuan
 * @since 2020-11-13 17:16:19
 */
@Service
public class DictionaryServiceImpl extends ServiceImpl<DictionaryMapper, Dictionary> implements DictionaryService {

}
