package cn.aiyuan.service.impl;

import cn.aiyuan.mapper.AccountMapper;
import cn.aiyuan.pojo.Account;
import cn.aiyuan.service.AccountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 账户表(Account)表服务实现类
 *
 * @author aiyuan
 * @since 2020-11-13 17:16:16
 */
@Service
public class AccountServiceImpl extends ServiceImpl<AccountMapper, Account> implements AccountService {

}
