package cn.aiyuan.service.impl;

import cn.aiyuan.mapper.PermissionMapper;
import cn.aiyuan.pojo.Permission;
import cn.aiyuan.service.PermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 权限表(Permission)表服务实现类
 *
 * @author aiyuan
 * @since 2020-11-13 17:16:22
 */
@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements PermissionService {

}
