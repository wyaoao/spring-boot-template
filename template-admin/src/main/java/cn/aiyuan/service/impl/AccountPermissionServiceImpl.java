package cn.aiyuan.service.impl;

import cn.aiyuan.mapper.AccountPermissionMapper;
import cn.aiyuan.pojo.AccountPermission;
import cn.aiyuan.service.AccountPermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 账户和权限中间表(AccountPermission)表服务实现类
 * @author aiyuan
 * @since 2020-11-13 17:16:18
 */
@Service
public class AccountPermissionServiceImpl extends ServiceImpl<AccountPermissionMapper, AccountPermission> implements AccountPermissionService {

}
