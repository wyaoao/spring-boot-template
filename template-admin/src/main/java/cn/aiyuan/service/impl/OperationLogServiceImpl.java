package cn.aiyuan.service.impl;

import cn.aiyuan.mapper.OperationLogMapper;
import cn.aiyuan.pojo.OperationLog;
import cn.aiyuan.service.OperationLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 操作日志表(OperationLog)表服务实现类
 *
 * @author aiyuan
 * @since 2020-11-13 17:16:21
 */
@Service
public class OperationLogServiceImpl extends ServiceImpl<OperationLogMapper, OperationLog> implements OperationLogService {

}
