package cn.aiyuan.service.impl;

import cn.aiyuan.mapper.RolePermissionMapper;
import cn.aiyuan.pojo.RolePermission;
import cn.aiyuan.service.RolePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 角色和权限中间表(RolePermission)表服务实现类
 *
 * @author aiyuan
 * @since 2020-11-13 17:16:24
 */
@Service
public class RolePermissionServiceImpl extends ServiceImpl<RolePermissionMapper, RolePermission> implements RolePermissionService {

}
