package cn.aiyuan.service.impl;

import cn.aiyuan.mapper.RoleMapper;
import cn.aiyuan.pojo.Role;
import cn.aiyuan.service.RoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 角色表(Role)表服务实现类
 *
 * @author aiyuan
 * @since 2020-11-13 17:16:23
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

}
