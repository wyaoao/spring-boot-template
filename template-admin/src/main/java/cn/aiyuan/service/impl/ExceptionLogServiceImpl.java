package cn.aiyuan.service.impl;

import cn.aiyuan.mapper.ExceptionLogMapper;
import cn.aiyuan.pojo.ExceptionLog;
import cn.aiyuan.service.ExceptionLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 异常日志表(ExceptionLog)表服务实现类
 *
 * @author aiyuan
 * @since 2020-11-13 17:16:20
 */
@Service
public class ExceptionLogServiceImpl extends ServiceImpl<ExceptionLogMapper, ExceptionLog> implements ExceptionLogService {

}
