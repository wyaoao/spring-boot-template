package cn.aiyuan.service;

import cn.aiyuan.pojo.Account;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 账户表(Account)表服务接口
 *
 * @author aiyuan
 * @since 2020-11-13 17:16:15
 */
public interface AccountService extends IService<Account> {

}
