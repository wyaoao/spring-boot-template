package cn.aiyuan.service;

import cn.aiyuan.pojo.Role;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 角色表(Role)表服务接口
 *
 * @author aiyuan
 * @since 2020-11-13 17:16:23
 */
public interface RoleService extends IService<Role> {

}
