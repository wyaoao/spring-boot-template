package cn.aiyuan.service;

import cn.aiyuan.pojo.ExceptionLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 异常日志表(ExceptionLog)表服务接口
 *
 * @author aiyuan
 * @since 2020-11-13 17:16:20
 */
public interface ExceptionLogService extends IService<ExceptionLog> {

}
