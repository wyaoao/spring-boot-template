package cn.aiyuan.service;

import cn.aiyuan.pojo.AccountPermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 账户和权限中间表(AccountPermission)表服务接口
 *
 * @author aiyuan
 * @since 2020-11-13 17:16:17
 */
public interface AccountPermissionService extends IService<AccountPermission> {

}
