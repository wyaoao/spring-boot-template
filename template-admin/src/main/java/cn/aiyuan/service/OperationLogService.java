package cn.aiyuan.service;

import cn.aiyuan.pojo.OperationLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 操作日志表(OperationLog)表服务接口
 *
 * @author aiyuan
 * @since 2020-11-13 17:16:21
 */
public interface OperationLogService extends IService<OperationLog> {

}
