package cn.aiyuan.service;

import cn.aiyuan.pojo.Permission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 权限表(Permission)表服务接口
 *
 * @author aiyuan
 * @since 2020-11-13 17:16:22
 */
public interface PermissionService extends IService<Permission> {

}
