package cn.aiyuan.service;

import cn.aiyuan.pojo.RolePermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 角色和权限中间表(RolePermission)表服务接口
 *
 * @author aiyuan
 * @since 2020-11-13 17:16:23
 */
public interface RolePermissionService extends IService<RolePermission> {

}
