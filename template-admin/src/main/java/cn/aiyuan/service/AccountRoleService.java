package cn.aiyuan.service;

import cn.aiyuan.pojo.AccountRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 账户和角色关联表(AccountRole)表服务接口
 *
 * @author aiyuan
 * @since 2020-11-13 17:16:18
 */
public interface AccountRoleService extends IService<AccountRole> {

}
