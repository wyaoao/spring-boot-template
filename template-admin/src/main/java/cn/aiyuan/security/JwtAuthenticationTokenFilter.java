package cn.aiyuan.security;


import cn.aiyuan.common.util.CommonUtils;
import cn.aiyuan.constant.GlobalConstant;
import cn.aiyuan.pojo.OperationLog;
import cn.aiyuan.service.OperationLogService;
import cn.aiyuan.utils.JwtTokenUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * JWT登录授权过滤器
 * https://github.com/shenzhuan/mallplus on 2018/4/26.
 * @author Administrator
 */
public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(JwtAuthenticationTokenFilter.class);

    @Resource
    public OperationLogService operationLogService;

    @Resource
    private UserDetailsService userDetailsService;

    @Resource
    private JwtTokenUtil jwtTokenUtil;

    @Value("${jwt.tokenHeader}")
    private String tokenHeader;

    @Value("${jwt.tokenHead}")
    private String tokenHead;


    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain chain) throws ServletException, IOException {
        long startTime, endTime;
        String requestType = request.getMethod();
        OperationLog sysLog = new OperationLog();
        StringBuffer sbParams = new StringBuffer();
        if (!HttpMethod.GET.name().equals(requestType)) {
            Map<String, String[]> params = new HashMap<>(request.getParameterMap());
            sbParams.append("?");
            for (String key : params.keySet()) {
                if (null == key || null == params.get(key) || null == params.get(key)[0]) {
                    continue;
                }
                sbParams.append(key).append("=").append(params.get(key)[0]).append("&");
            }
            if (sbParams.length() > 1) {
                sbParams.delete(sbParams.length() - 1, sbParams.length());
            }
            sysLog.setRequestParam(sbParams.toString());
        } else {
            sysLog.setRequestParam(getBodyString(request));
        }

        String fullUrl =  request.getRequestURL().toString();
        String username = null;
        String authHeader = request.getHeader(this.tokenHeader);
        if (StringUtils.isEmpty(authHeader)) {
            authHeader = request.getParameter(this.tokenHeader);
        }
        if (authHeader != null && authHeader.startsWith(this.tokenHead)) {
            String authToken = authHeader.substring(this.tokenHead.length());
            username = jwtTokenUtil.getUserNameFromToken(authToken);
            LOGGER.info("checking username:{}", username);
            if (fullUrl.contains(GlobalConstant.LOGOUT.getValue()) || fullUrl.contains(GlobalConstant.LOGIN.getValue())) {
                if (fullUrl.contains(GlobalConstant.LOGOUT.getValue())) {
                    SecurityContextHolder.getContext().setAuthentication(null);
                }
            } else {
                if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
                    UserDetails userDetails = this.userDetailsService.loadUserByUsername(username);
                    if (jwtTokenUtil.validateToken(authToken, userDetails)) {
                        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                        authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                        LOGGER.info("checking username:{}", username);
                        SecurityContextHolder.getContext().setAuthentication(authentication);
                    }
                }
            }

        }
        startTime = System.currentTimeMillis();
        chain.doFilter(request, response);
        endTime = System.currentTimeMillis();


        logger.info(formMapKey(username, fullUrl, requestType,
                CommonUtils.getClientIp(), sbParams.toString(), authHeader)
                + ",\"cost\":\"" + (endTime - startTime) + "ms\"");
        int startIntercept = fullUrl.replace("//", "a").indexOf("/") + 1;
        String interfaceName = fullUrl.substring(startIntercept);
        sysLog.setCreateTime(new Date());
        sysLog.setIp(CommonUtils.getClientIp());
        sysLog.setMethod(interfaceName);
        sysLog.setModule(requestType);

        sysLog.setDescription(authHeader);
        sysLog.setUserName(username);
        operationLogService.save(sysLog);
    }

    private String formMapKey(Object userId, String mothedName, String requestType,
                              String ip, String params, String token) {
        return "\"time\"" + ":\"" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS").format(new Date())
                + "\",\"name\"" + ":\"" + mothedName + "\",\"uid\":\"" + userId
                + "\",\"type\":\"" + requestType + "\",\"ip\":\"" + ip
                + "\",\"token\":\"" + token + "\",\"params\":\"" + params + "\"";
    }

    /**
     * 获取请求Body
     */
    public String getBodyString(final ServletRequest request) {
        StringBuilder sb = new StringBuilder();
        InputStream inputStream = null;
        BufferedReader reader = null;
        try {
            inputStream = cloneInputStream(request.getInputStream());
            reader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
            String line = "";
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();
    }

    /**
     * Description: 复制输入流</br>
     */
    public InputStream cloneInputStream(ServletInputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len;
        try {
            while ((len = inputStream.read(buffer)) > -1) {
                byteArrayOutputStream.write(buffer, 0, len);
            }
            byteArrayOutputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
    }
}
