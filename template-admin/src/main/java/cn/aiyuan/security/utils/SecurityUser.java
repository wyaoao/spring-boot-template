package cn.aiyuan.security.utils;


import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;


/**
 * @author Administrator
 */
public class SecurityUser extends User {


    private Long id;

    private Long infoId;

    private String accountNum;

    public SecurityUser(String accountNum, String password, Collection<? extends GrantedAuthority> authorities) {
        super(accountNum, password, authorities);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getInfoId() {
        return infoId;
    }

    public void setInfoId(Long infoId) {
        this.infoId = infoId;
    }

    public String getAccountNum() {
        return accountNum;
    }

    public void setAccountNum(String accountNum) {
        this.accountNum = accountNum;
    }
}
