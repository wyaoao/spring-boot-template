package cn.aiyuan.security.utils;

import lombok.Data;

/**
 * security 相关
 *
 * @author WuXiCheng
 * @version 1.0
 */
@Data
public class SecurityUserData {

    private Long id;

    private String accountNum;

}
