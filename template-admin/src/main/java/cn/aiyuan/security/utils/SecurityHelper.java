package cn.aiyuan.security.utils;

import org.springframework.beans.BeanUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.Assert;

/**
 * security 相关
 *
 * @author aiyuan
 * @version 1.0
 */
public class SecurityHelper {

    /**
     * 获取当前用户
     *
     */
    public static SecurityUserData getCurrentUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Assert.notNull(principal, "获取当前用户信息失败，请检查登录状态");
        SecurityUserData securityUserData = new SecurityUserData();
        BeanUtils.copyProperties(principal, securityUserData);
        return securityUserData;
    }

    /**
     * 获取当前用户id
     */
    public static Long getCurrentUserId() {
        return getCurrentUser().getId();
    }

    public static String getCurrentAccountNum() {
        return getCurrentUser().getAccountNum();
    }
}
