package cn.aiyuan.mapper;

import cn.aiyuan.pojo.Dictionary;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 字典表
 * (Dictionary)表数据库访问层
 *
 * @author aiyuan
 * @since 2020-11-13 17:16:19
 */
public interface DictionaryMapper extends BaseMapper<Dictionary> {

}
