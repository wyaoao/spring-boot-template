package cn.aiyuan.mapper;

import cn.aiyuan.pojo.AccountRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 账户和角色关联表(AccountRole)表数据库访问层
 *
 * @author aiyuan
 * @since 2020-11-13 17:16:18
 */
public interface AccountRoleMapper extends BaseMapper<AccountRole> {

}
