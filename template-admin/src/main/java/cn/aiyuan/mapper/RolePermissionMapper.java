package cn.aiyuan.mapper;

import cn.aiyuan.pojo.RolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 角色和权限中间表(RolePermission)表数据库访问层
 *
 * @author aiyuan
 * @since 2020-11-13 17:16:23
 */
public interface RolePermissionMapper extends BaseMapper<RolePermission> {

}
