package cn.aiyuan.mapper;

import cn.aiyuan.pojo.Permission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 权限表(Permission)表数据库访问层
 *
 * @author aiyuan
 * @since 2020-11-13 17:16:22
 */
public interface PermissionMapper extends BaseMapper<Permission> {

}
