package cn.aiyuan.mapper;

import cn.aiyuan.pojo.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 角色表(Role)表数据库访问层
 *
 * @author aiyuan
 * @since 2020-11-13 17:16:23
 */
public interface RoleMapper extends BaseMapper<Role> {

}
