package cn.aiyuan.mapper;

import cn.aiyuan.pojo.AccountPermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 账户和权限中间表(AccountPermission)表数据库访问层
 *
 * @author aiyuan
 * @since 2020-11-13 17:16:17
 */
public interface AccountPermissionMapper extends BaseMapper<AccountPermission> {

}
