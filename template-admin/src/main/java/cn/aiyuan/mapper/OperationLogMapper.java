package cn.aiyuan.mapper;

import cn.aiyuan.pojo.OperationLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 操作日志表(OperationLog)表数据库访问层
 *
 * @author aiyuan
 * @since 2020-11-13 17:16:21
 */
public interface OperationLogMapper extends BaseMapper<OperationLog> {

}
