package cn.aiyuan.mapper;


import cn.aiyuan.pojo.Account;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 账户表(Account)表数据库访问层
 *
 * @author aiyuan
 * @since 2020-11-13 17:16:14
 */
public interface AccountMapper extends BaseMapper<Account> {

}
