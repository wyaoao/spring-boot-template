package cn.aiyuan.mapper;


import cn.aiyuan.pojo.ExceptionLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 异常日志表(ExceptionLog)表数据库访问层
 *
 * @author aiyuan
 * @since 2020-11-13 17:16:20
 */
public interface ExceptionLogMapper extends BaseMapper<ExceptionLog> {

}
